from flask import Flask
import threading
import serial

arduino = serial.Serial(port='COM6', baudrate=9600, timeout=.1)

light_value = 0
led_inverted = False

app = Flask(__name__)


@app.route('/get-light')
def get_light():
    return str(light_value)

@app.route('/invert-led')
def invert_led():
    global led_inverted
    led_inverted = False if led_inverted else True
    return f"led inverted : {led_inverted}"



def startApp(app):
    app.run()


if __name__ == '__main__':
    t1 = threading.Thread(target=startApp, args=(app,))
    t1.start()
    while (True):
        data = arduino.readline()
        data_decode = data.decode()
        if data_decode != "":
            light_value = int(data_decode)
            led_mode = "LED_MODE 1" if led_inverted else "LED_MODE 0"
            arduino.write(bytes(led_mode, 'utf-8'))
            print(led_mode)
    t1.join()
