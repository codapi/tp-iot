#define LIGHT_DELAY_MS 500
#define LED_DURATION_MS 200

int lightSensorInputPin = A0;
int ledOutPin = 2;

unsigned long lastLightTime = 0;

// led state
bool isLedON = false;

// 0 == normal mode
// 1 == inversed mode
int ledBlinkingMode = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  
  Serial.begin(9600);
  pinMode(lightSensorInputPin, INPUT);
  pinMode(ledOutPin, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  unsigned long currentTime = millis();

  // Asynchronous light sensor reading
  if (lastLightTime + (unsigned long)LIGHT_DELAY_MS < currentTime) {
    int val = analogRead(lightSensorInputPin);   // read the light sensor value
    Serial.println(val);

    lastLightTime = currentTime;

    toggleLedState(ledBlinkingMode == 0);
  }

  // Automatically switch off the LED after its delay.
  if (lastLightTime + (unsigned long)LED_DURATION_MS < currentTime &&  isLedON == (ledBlinkingMode == 0)) {
    toggleLedState(ledBlinkingMode == 1);
  }

  // Automatically toggle the LED blinking mode.
  toggleLEDBlinkingMode();

}

// Change the state of the LED. Store the state in a variable.
void toggleLedState(bool isON) {
  if (isON == true) {
    digitalWrite(ledOutPin, HIGH);
    isLedON = true;
  } else {
    digitalWrite(ledOutPin, LOW);
    isLedON = false;
  }
}

// Read the Serial port to get the LED blinking mode.
int getLEDBlinkingMode()
{
  String rcv = Serial.readString();
  if (rcv.compareTo("") == 0) {
    return -1;
  }

  if (rcv.compareTo("LED_MODE 0") == 0) {
    return 0;
  }

  if (rcv.compareTo("LED_MODE 1") == 0) {
    return 1;
  }

  return -1;
}

// Toggle the LED blinking mode in function of the serial messages
void toggleLEDBlinkingMode() {
  int lbm = getLEDBlinkingMode();

  if (lbm == 0 || lbm == 1) {
    ledBlinkingMode = lbm;
  }
}
